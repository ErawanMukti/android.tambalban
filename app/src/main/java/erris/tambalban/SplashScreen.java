package erris.tambalban;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    SharedPreferences prefs;
    String auth_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        prefs   = getSharedPreferences("app", Context.MODE_PRIVATE);
        auth_id = prefs.getString("auth_id", "");

        Handler h = new Handler();
        h.postDelayed(s, 2000);
    }

    Runnable s = new Runnable() {
        @Override
        public void run() {
            Intent i;
            if (auth_id.equals("")) {
                i = new Intent(SplashScreen.this, MenuLogin.class);
            } else {
                i = new Intent(SplashScreen.this, MenuUtama.class);
            }

            startActivity(i);
            finish();
        }
    };
}
