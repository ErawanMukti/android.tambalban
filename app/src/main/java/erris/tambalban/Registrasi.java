package erris.tambalban;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Registrasi extends AppCompatActivity {

    String ip_address;
    EditText txtNama, txtPhone, txtEmail, txtPassword;
    CheckBox chkPemilik;
    Button btnSave;

    @Override
    public boolean onKeyDown (int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(getBaseContext(), MenuLogin.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        ip_address  = getResources().getString(R.string.ip_address);
        txtNama     = (EditText) findViewById(R.id.txtRegNama);
        txtPhone    = (EditText) findViewById(R.id.txtRegPhone);
        txtEmail    = (EditText) findViewById(R.id.txtRegEmail);
        txtPassword = (EditText) findViewById(R.id.txtRegPassword);
        chkPemilik  = (CheckBox) findViewById(R.id.chkPemilik);
        btnSave     = (Button) findViewById(R.id.btnRegSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNama.getText().equals("")) {
                    Toast.makeText(getBaseContext(), "Kolom nama tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(txtEmail.getText().equals("")) {
                    Toast.makeText(getBaseContext(), "Kolom email tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(txtPassword.getText().equals("")) {
                    Toast.makeText(getBaseContext(), "Kolom password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString()).matches()){
                    Toast.makeText(getBaseContext(), "Format email yang anda masukkan salah, silahkan ulangi", Toast.LENGTH_SHORT).show();
                    return;
                }

                register(txtNama.getText().toString(), txtEmail.getText().toString(), txtPassword.getText().toString(), txtPhone.getText().toString(), chkPemilik.isChecked());
            }
        });

    }

    private void register(String nama, String email, String password, String telepon, Boolean status) {
        class ClsAsync extends AsyncTask<String, Void, String> {
            private Dialog loadingDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadingDialog = ProgressDialog.show(Registrasi.this, "Harap Tunggu", "Loading...");
            }

            @Override
            protected String doInBackground(String... params) {
                String nama     = params[0];
                String email    = params[1];
                String pass     = params[2];
                String telepon  = params[3];
                String status   = params[4];

                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("nama", nama));
                nameValuePairs.add(new BasicNameValuePair("email", email));
                nameValuePairs.add(new BasicNameValuePair("pass", pass));
                nameValuePairs.add(new BasicNameValuePair("telepon", telepon));
                nameValuePairs.add(new BasicNameValuePair("status", status));

                String result = null;
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(ip_address + "register.php");
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                loadingDialog.dismiss();

                if (result.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, silahkan ulangi", Toast.LENGTH_LONG).show();
                } else {
                    String s = result.trim();
                    if (s.equalsIgnoreCase("Berhasil")) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(Registrasi.this);
                        dialog.setCancelable(false);
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                Intent i = new Intent(Registrasi.this, Login.class);
                                startActivity(i);
                                finish();
                                return;
                            }

                        });

                        dialog.setMessage("Selamat. Pendaftaran Berhasil, Silahkan Kembali Ke Halaman Login");
                        AlertDialog alert = dialog.create();
                        alert.show();
                    } else {
                        //Development
                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();

                        //Production
                        //Toast.makeText(getApplicationContext(), "Pendaftaran gagal. Harap periksa", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
        ClsAsync task = new ClsAsync();
        task.execute(nama, email, password, telepon, status.toString());
    }
}
