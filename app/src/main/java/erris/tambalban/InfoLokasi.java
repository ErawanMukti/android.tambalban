package erris.tambalban;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class InfoLokasi extends AppCompatActivity {

    SharedPreferences prefs;
    String ip_address;
    String myJSON, id, auth_akses;
    JSONArray data = null;
    TextView lblAlamat, lblJenis;
    Button btnArah, btnPesan, btnValid, btnReject, btnHapus;
    MapView mpLokasi;
    LinearLayout lay;
    Double lat, lng;
    Bundle extras;
    String id_tambal_ban, id_member, alamat, str_jenis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_lokasi);

        prefs       = getSharedPreferences("app", Context.MODE_PRIVATE);
        extras      = getIntent().getExtras();
        id          = extras.getString("id");
        ip_address  = getResources().getString(R.string.ip_address);
        mpLokasi    = (MapView) findViewById(R.id.mapILokasiMap);
        lblAlamat   = (TextView) findViewById(R.id.lblILokasiAlamat);
        lblJenis    = (TextView) findViewById(R.id.lblILokasiJenis);
        btnArah     = (Button) findViewById(R.id.btnILokasiDirection);
        btnPesan    = (Button) findViewById(R.id.btnILokasiPesan);
        btnValid    = (Button) findViewById(R.id.btnILokasiValid);
        btnReject   = (Button) findViewById(R.id.btnILokasiReject);
        btnHapus    = (Button) findViewById(R.id.btnILokasiHapus);
        lay         = (LinearLayout) findViewById(R.id.layoutILokasiButton);
        auth_akses  = prefs.getString("auth_akses", "");

        mpLokasi.onCreate(savedInstanceState);

        if (!auth_akses.equals("admin")) {
            lay.setVisibility(View.GONE);
        }

        btnArah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lat == 0 || lng == 0) {
                    Toast.makeText(getBaseContext(), "Tidak dapat mendapatkan petunjuk arah", Toast.LENGTH_SHORT).show();
                    return;
                }

                Uri uri = Uri.parse("google.navigation:q=" + lat + "," + lng);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                i.setPackage("com.google.android.apps.maps");
                startActivity(i);
            }
        });

        btnPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id.equals(id_member)) {
                    Toast.makeText(getBaseContext(), "", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(getBaseContext(), ChatPesan.class);
                    i.putExtra("id_pesan", "");
                    i.putExtra("pengirim", id);
                    i.putExtra("penerima", id_member);
                    startActivity(i);
                }
            }
        });

        btnValid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_status_lokasi("V");
            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_status_lokasi("R");
            }
        });

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                update_status_lokasi("H");
            }
        });

        GetDataJSON task = new GetDataJSON();
        task.execute();
    }

    @Override
    protected void onResume() {
        mpLokasi.onResume();
        super.onResume();
    }

    class GetDataJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(InfoLokasi.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("id", id));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "get_lokasi_info.php");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            loadingDialog.dismiss();

            if (result.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Data lokasi tidak ditemukan", Toast.LENGTH_LONG).show();
            } else {
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);

                    data = jsonObj.getJSONArray("hasil");
                    JSONObject c = data.getJSONObject(0);

                    id_tambal_ban = c.getString("id_tambal_ban");
                    id_member = c.getString("id_member");
                    alamat = c.getString("alamat") + "\n" + c.getString("no_telp") + "(" + c.getString("jamb") + " - " + c.getString("jamt") + ")";
                    lat = c.getDouble("lat");
                    lng = c.getDouble("lng");

                    lblAlamat.setText(alamat);

                    if (id_member.equals("")) {
                        btnPesan.setVisibility(View.GONE);
                    }

                    str_jenis = "Menerima Tambal Ban:\n";
                    data = jsonObj.getJSONArray("arr_tb");
                    for(int i=0; i<data.length(); i++){
                        JSONObject obj_jenis = data.getJSONObject(i);
                        str_jenis += obj_jenis.getString("nama") + "\n";
                    }

                    str_jenis += "\nMenerima Jenis Kendaraan:\n";
                    data = jsonObj.getJSONArray("arr_jk");
                    for(int i=0; i<data.length(); i++){
                        JSONObject obj_jenis = data.getJSONObject(i);
                        str_jenis += obj_jenis.getString("nama") + "\n";
                    }
                    lblJenis.setText(str_jenis);

                    mpLokasi.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            LatLng posisi = new LatLng(lat, lng);
                            googleMap.addMarker(new MarkerOptions().position(posisi).title("Posisi Lokasi"));
                            googleMap.setMyLocationEnabled(true);
                            googleMap.getUiSettings().setZoomControlsEnabled(true);
                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(posisi));
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(posisi, 20.0f));
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void update_status_lokasi(String status) {
        class ClsAsync extends AsyncTask<String, Void, String> {
            private Dialog loadingDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadingDialog = ProgressDialog.show(InfoLokasi.this, "Harap Tunggu", "Loading...");
            }

            @Override
            protected String doInBackground(String... params) {
                String status = params[0];

                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", id));
                nameValuePairs.add(new BasicNameValuePair("status", status));

                String result = null;
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(ip_address + "update_status_lokasi.php");
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                loadingDialog.dismiss();

                if (result.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, silahkan ulangi", Toast.LENGTH_LONG).show();
                } else {
                    String s = result.trim();
                    if (s.equalsIgnoreCase("Berhasil")) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(InfoLokasi.this);
                        dialog.setCancelable(false);
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                finish();
                                return;
                            }

                        });

                        dialog.setMessage("Selamat. Perubahan status lokasi berhasil");
                        AlertDialog alert = dialog.create();
                        alert.show();
                    } else {
                        //Development
                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();

                        //Production
                        //Toast.makeText(getApplicationContext(), "Pendaftaran gagal. Harap periksa", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
        ClsAsync task = new ClsAsync();
        task.execute(status);
    }
}

