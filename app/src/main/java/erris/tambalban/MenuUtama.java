package erris.tambalban;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuUtama extends AppCompatActivity {

    SharedPreferences prefs;
    Button btnCari, btnTambah, btnPesan, btnValidasi, btnJenisTb, btnJenisKendaraan, btnLogout;
    String auth_akses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);

        prefs       = getSharedPreferences("app", Context.MODE_PRIVATE);
        btnCari     = (Button) findViewById(R.id.btnMenuCari);
        btnTambah   = (Button) findViewById(R.id.btnMenuTambah);
        btnPesan    = (Button) findViewById(R.id.btnMenuPesan);
        btnValidasi = (Button) findViewById(R.id.btnMenuValidasi);
        btnJenisTb  = (Button) findViewById(R.id.btnMenuJenisTb);
        btnJenisKendaraan = (Button) findViewById(R.id.btnMenuJenisKendaraan);
        btnLogout   = (Button) findViewById(R.id.btnMenuLogout);
        auth_akses  = prefs.getString("auth_akses", "");

        if (auth_akses.equals("guest")) {
            btnTambah.setVisibility(View.GONE);
            btnPesan.setVisibility(View.GONE);
        }
        if (auth_akses.equals("admin")) {
            btnValidasi.setVisibility(View.VISIBLE);
            btnJenisTb.setVisibility(View.VISIBLE);
            btnJenisKendaraan.setVisibility(View.VISIBLE);
        }

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), CariLokasi.class);
                startActivity(i);
            }
        });

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), TambahLokasi.class);
                startActivity(i);
            }
        });

        btnPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), ListPesan.class);
                startActivity(i);
            }
        });

        btnValidasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), ListLokasi.class);
                startActivity(i);
            }
        });

        btnJenisTb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getBaseContext(), ListJenisTambalBan.class);
                startActivity(i);
            }
        });

        btnJenisKendaraan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getBaseContext(), ListJenisKendaraan.class);
                startActivity(i);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor e = prefs.edit();
                e.putString("auth_id", "");
                e.putString("auth_name", "");
                e.putString("auth_akses", "");
                e.commit();

                Intent i = new Intent(getBaseContext(), MenuLogin.class);
                startActivity(i);
                finish();
            }
        });
    }
}
