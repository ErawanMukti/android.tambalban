package erris.tambalban;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class Login extends AppCompatActivity {

    SharedPreferences prefs;
    String ip_address, myJSON;
    EditText txtUname, txtUpass;
    Button btnLogin;
    JSONArray data = null;

    @Override
    public boolean onKeyDown (int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent i = new Intent(getBaseContext(), MenuLogin.class);
            startActivity(i);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        prefs       = getSharedPreferences("app", Context.MODE_PRIVATE);
        ip_address  = getResources().getString(R.string.ip_address);
        txtUname    = (EditText) findViewById(R.id.txtLoginEmail);
        txtUpass    = (EditText) findViewById(R.id.txtLoginPassword);
        btnLogin    = (Button) findViewById(R.id.btnLoginSave);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUname.equals("") || txtUpass.equals("")){
                    Toast.makeText(getBaseContext(), "Kolom email dan password tidak boleh kosong, silahkan ulangi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(txtUname.getText().toString().equals("admin@admin.com") && txtUpass.getText().toString().equals("1234")){
                    SharedPreferences.Editor e = prefs.edit();
                    e.putString("auth_id", "admin");
                    e.putString("auth_name", "Admin");
                    e.putString("auth_akses", "admin");
                    e.commit();

                    Intent i = new Intent(Login.this, MenuUtama.class);
                    startActivity(i);
                    finish();
                    return;
                }else{
                    login(txtUname.getText().toString(), txtUpass.getText().toString());
                }
            }
        });
    }

    private void login(String email, String password) {
        class ClsAsync extends AsyncTask<String, Void, String> {
            private Dialog loadingDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadingDialog = ProgressDialog.show(Login.this, "Harap Tunggu", "Loading...");
            }

            @Override
            protected String doInBackground(String... params) {
                String email    = params[0];
                String pass     = params[1];

                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("email", email));
                nameValuePairs.add(new BasicNameValuePair("password", pass));

                String result = null;
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(ip_address + "login.php");
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                loadingDialog.dismiss();

                if (result.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, silahkan ulangi", Toast.LENGTH_LONG).show();
                } else {
                    String s = result.trim();
                    myJSON = s;

                    try {
                        JSONObject jsonObj = new JSONObject(myJSON);
                        data = jsonObj.getJSONArray("hasil");
                        JSONObject jData = data.getJSONObject(0);

                        String status, id, nama, akses;
                        status = jData.getString("status");

                        if (status.equals("Berhasil")) {
                            id    = jData.getString("id_member");
                            nama  = jData.getString("nama");
                            akses = jData.getString("pemilik");

                            SharedPreferences.Editor e = prefs.edit();
                            e.putString("auth_id", id);
                            e.putString("auth_name", nama);
                            e.putString("auth_akses", akses);
                            e.commit();

                            Intent i = new Intent(Login.this, MenuUtama.class);
                            startActivity(i);
                            finish();
                        } else if(status.equals("Gagal")){
                            Toast.makeText(getApplicationContext(), "Email / password yang anda masukkan salah, silahkan ulangi", Toast.LENGTH_LONG).show();
                        }else {
                            //Development
                            Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();

                            //Production
                            //Toast.makeText(getApplicationContext(), "Pendaftaran gagal. Harap periksa", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        //e.printStackTrace();
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }
            }
        }
        ClsAsync task = new ClsAsync();
        task.execute(email, password);
    }

}
