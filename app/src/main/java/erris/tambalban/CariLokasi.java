package erris.tambalban;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CariLokasi extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    Button btnFilterJk, btnFilterTb;
    ListView lv;
    TextView lblError;
    private String myJSON;
    JSONArray data = null;
    String ip_address;
    String filterJk, filterTb;
    ArrayList selected_jk, selected_tb;
    CharSequence[] arr_id_tb, arr_nama_tb;
    String[] arr_id_jk, arr_nama_jk;
    int[] arr_icon_jk;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Double curLat, curLng;
    boolean[] checkValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_lokasi);

        ip_address  = getResources().getString(R.string.ip_address);
        btnFilterJk = (Button) findViewById(R.id.btnCLokasiFilterJk);
        btnFilterTb = (Button) findViewById(R.id.btnCLokasiFilterTb);
        lv          = (ListView) findViewById(R.id.listCariLokasi);
        lblError    = (TextView) findViewById(R.id.lblCLokasiError);
        filterJk    = "";
        filterTb    = "";
        selected_jk = new ArrayList();
        selected_tb = new ArrayList();
        curLat      = 0.00;
        curLng      = 0.00;

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(CariLokasi.this)
                    .addConnectionCallbacks(CariLokasi.this)
                    .addOnConnectionFailedListener(CariLokasi.this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }

        btnFilterJk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValue = new boolean[arr_id_jk.length];
                selected_jk = new ArrayList<String>();
                /*for(int i=0; i<arr_id_jk.length; i++) {
                    if (selected_jk.contains(arr_id_jk[i])) {
                        checkValue[i] = true;
                    } else {
                        checkValue[i] = false;
                    }
                }*/

                AlertDialog dialog = new AlertDialog.Builder(CariLokasi.this)
                        .setAdapter(new CheckJkAdapter(CariLokasi.this, arr_nama_jk, arr_icon_jk), null)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                filterJk = "";
                                for (int i=0; i<selected_jk.size(); i++) {
                                    filterJk += selected_jk.get(i);

                                    if (i<selected_jk.size() - 1) {
                                        filterJk += ",";
                                    }
                                }
                                GetDataJSON task = new GetDataJSON();
                                task.execute();
                            }
                        }).create();

                ListView lvDialog = dialog.getListView();
                lvDialog.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                lvDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        CheckedTextView ctxt = (CheckedTextView) view.findViewById(R.id.txtCheckListJenis);
                        boolean isChecked = ctxt.isChecked();

                        ctxt.setChecked(!isChecked);

                        if (!isChecked) {
                            selected_jk.add(arr_id_jk[i]);
                        } else {
                            if (selected_jk.contains(arr_id_jk[i])) {
                                selected_jk.remove(arr_id_jk[i]);
                            }
                        }
                    }
                });

                dialog.show();

            }
        });

        btnFilterTb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean[] checkValue = new boolean[arr_id_tb.length];
                for(int i=0; i<arr_id_tb.length; i++) {
                    if (selected_tb.contains(arr_id_tb[i])) {
                        checkValue[i] = true;
                    } else {
                        checkValue[i] = false;
                    }
                }

                AlertDialog dialog = new AlertDialog.Builder(CariLokasi.this)
                        .setTitle("Filter Jenis Tambal Ban").setMultiChoiceItems(arr_nama_tb, checkValue, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                if (isChecked) {
                                    selected_tb.add(arr_id_tb[which]);
                                } else {
                                    if (selected_tb.contains(arr_id_tb[which])) {
                                        selected_tb.remove(arr_id_tb[which]);
                                    }
                                }
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                filterTb = "";
                                for (int i=0; i<selected_tb.size(); i++) {
                                    filterTb += selected_tb.get(i);

                                    if (i<selected_tb.size() - 1) {
                                        filterTb += ",";
                                    }
                                }
                                GetDataJSON task = new GetDataJSON();
                                task.execute();
                            }
                        }).create();
                dialog.show();
            }
        });

        lblError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetDataJSON task = new GetDataJSON();
                task.execute();
            }
        });

        GetDataJSON task = new GetDataJSON();
        task.execute();
    }

    private double hitungJarak(LatLng fromPosition, LatLng toPosition) {
        double dLat = Math.toRadians(toPosition.latitude - fromPosition.latitude);
        double dLon = Math.toRadians(toPosition.longitude - fromPosition.longitude);

        double lat1 = Math.toRadians(fromPosition.latitude);
        double lat2 = Math.toRadians(toPosition.latitude);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        double R = 6371000;

        return R * c;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            curLat = mLastLocation.getLatitude();
            curLng = mLastLocation.getLongitude();
        } else {
            curLat = 0.00;
            curLng = 0.00;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    class GetDataJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(CariLokasi.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("filter_jk", filterJk));
            nameValuePairs.add(new BasicNameValuePair("filter_tb", filterTb));

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "cari_lokasi.php");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data lokasi tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                if (curLat == 0.00 || curLng == 0.00) {
                    lblError.setVisibility(View.VISIBLE);
                    return;
                }

                lblError.setVisibility(View.GONE);
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("hasil");

                    final String[] id     = new String[data.length()];
                    final String[] alamat = new String[data.length()];
                    final String[] nama   = new String[data.length()];
                    final String[] status = new String[data.length()];

                    for(int i=0; i<data.length(); i++){
                        Double lat, lng, jarak;
                        JSONObject c = data.getJSONObject(i);

                        lat = c.getDouble("lat");
                        lng = c.getDouble("lng");
                        jarak = hitungJarak(new LatLng(curLat, curLng), new LatLng(lat, lng));

                        if (jarak < 1000) {
                            id[i]     = c.getString("id_tambal_ban");
                            alamat[i] = c.getString("alamat");
                            nama[i]   = c.getString("no_telp") + "(" + c.getString("jamb") + "-" + c.getString("jamb") + "), Tarif: " + c.getString("tarif");
                            status[i] = String.format("%.2f m", jarak) ;
                        }
                    }

                    final ListViewAdapter adapter = new ListViewAdapter(CariLokasi.this, alamat, nama, status);
                    lv.setAdapter(adapter);

                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long idx) {
                            String id_lokasi = id[position];

                            Intent i = new Intent(CariLokasi.this, InfoLokasi.class);
                            i.putExtra("id", id_lokasi);
                            startActivity(i);
                            finish();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GetJenisJSON task = new GetJenisJSON();
                task.execute();
            }
        }
    }

    class GetJenisJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(CariLokasi.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "get_all_jenis.php");
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data jenis tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("arr_jk");

                    arr_id_jk   = new String[data.length()];
                    arr_nama_jk = new String[data.length()];
                    arr_icon_jk = new int[data.length()];

                    for(int i=0; i<data.length(); i++){
                        JSONObject c   = data.getJSONObject(i);
                        arr_id_jk[i]   = c.getString("id_jenis");
                        arr_nama_jk[i] = c.getString("nama_jenis");
                        arr_icon_jk[i] = c.getInt("icon");
                    }

                    data = jsonObj.getJSONArray("arr_tb");

                    arr_id_tb   = new String[data.length()];
                    arr_nama_tb = new String[data.length()];

                    for(int i=0; i<data.length(); i++){
                        JSONObject c   = data.getJSONObject(i);
                        arr_id_tb[i]   = c.getString("id_jenis");
                        arr_nama_tb[i] = c.getString("nama_jenis");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
