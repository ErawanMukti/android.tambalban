package erris.tambalban;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ChatAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final ArrayList<String> pesan, tanggal;
    private final ArrayList<Integer> gambar;

    public ChatAdapter(Activity context, ArrayList<String> tanggal, ArrayList<String> pesan, ArrayList<Integer> gambar) {
        super(context, R.layout.listview_chat_list, pesan);
        this.context = context;
        this.tanggal = tanggal;
        this.pesan   = pesan;
        this.gambar  = gambar;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getView(position, convertView, parent);
    }
    
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View rowView= inflater.inflate(R.layout.listview_chat_list, null, true);
        rowView.setMinimumHeight(60);

        TextView txtTanggal = (TextView) rowView.findViewById(R.id.lblCPesanTanggal);
        TextView txtPesan   = (TextView) rowView.findViewById(R.id.lblCPesanMessage);
        LinearLayout lay    = (LinearLayout) rowView.findViewById(R.id.layoutChatPesan);
        RelativeLayout layP = (RelativeLayout) rowView.findViewById(R.id.layoutChatPesanParent);

        txtTanggal.setText(tanggal.get(position));
        txtPesan.setText(pesan.get(position));
        lay.setBackgroundResource(gambar.get(position));

        if (gambar.get(position) == R.drawable.out_message_bg) {
            layP.setGravity(Gravity.LEFT);
            txtTanggal.setTextColor(Color.parseColor("#FFFFFF"));
            txtPesan.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            layP.setGravity(Gravity.RIGHT);
            txtTanggal.setTextColor(Color.parseColor("#4A5865"));
            txtPesan.setTextColor(Color.parseColor("#4A5865"));
        }

        return rowView;
    }

    public void add(String tanggal, String pesan, Integer gambar) {
        this.tanggal.add(tanggal);
        this.pesan.add(pesan);
        this.gambar.add(gambar);
    }
}
