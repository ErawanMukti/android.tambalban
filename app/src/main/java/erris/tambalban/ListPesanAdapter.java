package erris.tambalban;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListPesanAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String id_member;
    private final String[] pengirim, penerima, nama_pengirim, nama_penerima, pesan, tanggal;

    public ListPesanAdapter(Activity context, String id_member, String[] pengirim, String[] penerima, String[] nama_pengirim, String[] nama_penerima, String[] pesan, String[] tanggal) {
        super(context, R.layout.listview_pesan_list, pengirim);
        this.context = context;

        this.id_member      = id_member;
        this.pengirim       = pengirim;
        this.penerima       = penerima;
        this.nama_pengirim  = nama_pengirim;
        this.nama_penerima  = nama_penerima;
        this.pesan          = pesan;
        this.tanggal        = tanggal;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getView(position, convertView, parent);
    }
    
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View rowView= inflater.inflate(R.layout.listview_pesan_list, null, true);
        rowView.setMinimumHeight(60);

        TextView txtNama    = (TextView) rowView.findViewById(R.id.txtListPesanNama);
        TextView txtPesan   = (TextView) rowView.findViewById(R.id.txtListPesanIsi);
        TextView txtTanggal = (TextView) rowView.findViewById(R.id.txtListPesanTanggal);

        if (id_member.equals(pengirim[position])) {
            txtNama.setText(nama_penerima[position]);
        } else {
            txtNama.setText(nama_pengirim[position]);
        }
        txtPesan.setText(pesan[position]);
        txtTanggal.setText(tanggal[position]);

        return rowView;
    }

}
