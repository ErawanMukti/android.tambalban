package erris.tambalban;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MenuLogin extends AppCompatActivity {

    SharedPreferences prefs;
    ImageView img;
    int idx;
    Button btnLogin, btnReg, btnGuest;
    boolean isGPSEnabled = false, isNetworkEnabled = false;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_login);

        prefs    = getSharedPreferences("app", Context.MODE_PRIVATE);
        idx      = prefs.getInt("idx", 1);
        // img      = (ImageView) findViewById(R.id.imgMenuLogin);
        btnLogin = (Button)  findViewById(R.id.btnLogin);
        btnReg   = (Button)  findViewById(R.id.btnRegister);
        btnGuest = (Button)  findViewById(R.id.btnGuest);

        /*if (idx == 1) {
            img.setImageResource(R.drawable.wallpaper_satu);
            idx++;
        } else if (idx == 2) {
            img.setImageResource(R.drawable.wallpaper_dua);
            idx++;
        } else {
            img.setImageResource(R.drawable.wallpaper_tiga);
            idx = 1;
        }

        SharedPreferences.Editor e = prefs.edit();
        e.putInt("idx", idx);
        e.commit();*/

        //checkNetwork();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), Login.class);
                startActivity(i);
                finish();
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), Registrasi.class);
                startActivity(i);
                finish();
            }
        });

        btnGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor e = prefs.edit();
                e.putString("auth_id", "guest");
                e.putString("auth_name", "Guest");
                e.putString("auth_akses", "guest");
                e.commit();

                Intent i = new Intent(getBaseContext(), MenuUtama.class);
                startActivity(i);
                finish();
            }
        });
    }

    public void checkNetwork() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        if (!isGPSEnabled) {
            showGPSAlert();
            return;
        }

        if (!isNetworkEnabled) {
            showSettingsNetworkAlert();
            return;
        }

    }

    public void showGPSAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getBaseContext());

        //Setting Dialog Title
        alertDialog.setTitle("GPS settings");

        //Setting Dialog Message
        alertDialog.setMessage("GPS tidak aktif. Mohon aktifkan GPS di menu setting");

        //On Pressing Setting button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        //On pressing cancel button
        alertDialog.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                System.exit(1);
            }
        });

        alertDialog.show();
    }

    public void showSettingsNetworkAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getBaseContext());

        // Setting Dialog Title
        alertDialog.setTitle("Internet settings");

        // Setting Dialog Message
        alertDialog.setMessage("Internet tidak aktif. Mohon aktifkan Internet di menu setting");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                System.exit(1);
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

}
