package erris.tambalban;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ListLokasi extends AppCompatActivity {

    Button btnFilterJk, btnFilterTb;
    ListView lv;
    private String myJSON;
    JSONArray data = null;
    String ip_address;
    String filterJk, filterTb;
    ArrayList selected_jk, selected_tb;
    CharSequence[] arr_id_tb, arr_nama_tb;
    String[] arr_id_jk, arr_nama_jk;
    int[] arr_icon_jk;
    boolean[] checkValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_lokasi);

        ip_address  = getResources().getString(R.string.ip_address);
        btnFilterJk = (Button) findViewById(R.id.btnLLokasiFilterJk);
        btnFilterTb = (Button) findViewById(R.id.btnLLokasiFilterTb);
        lv          = (ListView) findViewById(R.id.listLokasi);
        filterJk    = "";
        filterTb    = "";
        selected_jk = new ArrayList();
        selected_tb = new ArrayList();

        btnFilterJk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValue = new boolean[arr_id_jk.length];
                selected_jk = new ArrayList<String>();
                /*for(int i=0; i<arr_id_jk.length; i++) {
                    if (selected_jk.contains(arr_id_jk[i])) {
                        checkValue[i] = true;
                    } else {
                        checkValue[i] = false;
                    }
                }*/

                AlertDialog dialog = new AlertDialog.Builder(ListLokasi.this)
                        .setAdapter(new CheckJkAdapter(ListLokasi.this, arr_nama_jk, arr_icon_jk), null)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                filterJk = "";
                                for (int i=0; i<selected_jk.size(); i++) {
                                    filterJk += selected_jk.get(i);

                                    if (i<selected_jk.size() - 1) {
                                        filterJk += ",";
                                    }
                                }
                                GetDataJSON task = new GetDataJSON();
                                task.execute();
                            }
                        }).create();

                ListView lvDialog = dialog.getListView();
                lvDialog.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                lvDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        CheckedTextView ctxt = (CheckedTextView) view.findViewById(R.id.txtCheckListJenis);
                        boolean isChecked = ctxt.isChecked();

                        ctxt.setChecked(!isChecked);

                        if (!isChecked) {
                            selected_jk.add(arr_id_jk[i]);
                        } else {
                            if (selected_jk.contains(arr_id_jk[i])) {
                                selected_jk.remove(arr_id_jk[i]);
                            }
                        }
                    }
                });

                dialog.show();
            }
        });

        btnFilterTb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean[] checkValue = new boolean[arr_id_tb.length];
                for(int i=0; i<arr_id_tb.length; i++) {
                    if (selected_tb.contains(arr_id_tb[i])) {
                        checkValue[i] = true;
                    } else {
                        checkValue[i] = false;
                    }
                }

                AlertDialog dialog = new AlertDialog.Builder(ListLokasi.this)
                        .setTitle("Filter Jenis Tambal Ban").setMultiChoiceItems(arr_nama_tb, checkValue, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                if (isChecked) {
                                    selected_tb.add(arr_id_tb[which]);
                                } else {
                                    if (selected_tb.contains(arr_id_tb[which])) {
                                        selected_tb.remove(arr_id_tb[which]);
                                    }
                                }
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                filterTb = "";
                                for (int i=0; i<selected_tb.size(); i++) {
                                    filterTb += selected_tb.get(i);

                                    if (i<selected_tb.size() - 1) {
                                        filterTb += ",";
                                    }
                                }
                                GetDataJSON task = new GetDataJSON();
                                task.execute();
                            }
                        }).create();
                dialog.show();
            }
        });

        GetDataJSON task = new GetDataJSON();
        task.execute();
    }

    class GetDataJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(ListLokasi.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("filter_jk", filterJk));
            nameValuePairs.add(new BasicNameValuePair("filter_tb", filterTb));

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "get_lokasi.php");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data lokasi tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("hasil");

                    final String[] id     = new String[data.length()];
                    final String[] alamat = new String[data.length()];
                    final String[] nama   = new String[data.length()];
                    final String[] status = new String[data.length()];

                    for(int i=0; i<data.length(); i++){
                        JSONObject c = data.getJSONObject(i);
                        id[i]     = c.getString("id_tambal_ban");
                        alamat[i] = c.getString("alamat");
                        nama[i]   = c.getString("no_telp") + "(" + c.getString("jamb") + "-" + c.getString("jamb") + "), Tarif: " + c.getString("tarif");
                        status[i] = c.getString("status");
                    }

                    final ListViewAdapter adapter = new ListViewAdapter(ListLokasi.this, alamat, nama, status);
                    lv.setAdapter(adapter);

                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long idx) {
                            String id_lokasi = id[position];

                            Intent i = new Intent(ListLokasi.this, InfoLokasi.class);
                            i.putExtra("id", id_lokasi);
                            startActivity(i);
                            finish();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GetJenisJSON task = new GetJenisJSON();
                task.execute();
            }
        }
    }

    class GetJenisJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(ListLokasi.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "get_all_jenis.php");
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data jenis tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("arr_jk");

                    arr_id_jk   = new String[data.length()];
                    arr_nama_jk = new String[data.length()];
                    arr_icon_jk = new int[data.length()];

                    for(int i=0; i<data.length(); i++){
                        JSONObject c   = data.getJSONObject(i);
                        arr_id_jk[i]   = c.getString("id_jenis");
                        arr_nama_jk[i] = c.getString("nama_jenis");
                        arr_icon_jk[i] = c.getInt("icon");
                    }

                    data = jsonObj.getJSONArray("arr_tb");

                    arr_id_tb   = new String[data.length()];
                    arr_nama_tb = new String[data.length()];

                    for(int i=0; i<data.length(); i++){
                        JSONObject c   = data.getJSONObject(i);
                        arr_id_tb[i]   = c.getString("id_jenis");
                        arr_nama_tb[i] = c.getString("nama_jenis");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
