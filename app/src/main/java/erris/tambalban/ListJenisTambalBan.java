package erris.tambalban;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class ListJenisTambalBan extends AppCompatActivity {

    String ip_address;
    ListView lv;
    Button btnTambah;
    private String myJSON;
    JSONArray data = null;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_jenis_tambal_ban);

        ip_address = getResources().getString(R.string.ip_address);
        lv         = (ListView) findViewById(R.id.listJenisTb);
        btnTambah  = (Button)findViewById(R.id.btnListTbTambah);

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ListJenisTambalBan.this, JenisTambalBanInfo.class);
                i.putExtra("id_jenis", "");
                startActivity(i);
            }
        });

        GetDataJSON task = new GetDataJSON();
        task.execute();
    }

    class GetDataJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(ListJenisTambalBan.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("id", "%"));

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "get_jenis_tb.php");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data jenis tambal ban tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("hasil");

                    final String[] id_tb   = new String[data.length()];
                    final String[] nama_tb = new String[data.length()];

                    for(int i=0; i<data.length(); i++){
                        JSONObject c = data.getJSONObject(i);
                        id_tb[i]     = c.getString("id_jenis");
                        nama_tb[i]   = c.getString("nama_jenis");
                    }

                    adapter = new ArrayAdapter(ListJenisTambalBan.this, android.R.layout.simple_list_item_1, nama_tb);
                    lv.setAdapter(adapter);

                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent i = new Intent(ListJenisTambalBan.this, JenisTambalBanInfo.class);
                            i.putExtra("id_jenis", id_tb[position]);
                            startActivity(i);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
