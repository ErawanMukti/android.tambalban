package erris.tambalban;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CheckJkAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] ket;
    private final int[] gbr;

    public CheckJkAdapter(Activity context, String[] keterangan, int[] gambar) {
        super(context, R.layout.listview_pesan_list, keterangan);
        this.context = context;

        this.ket = keterangan;
        this.gbr = gambar;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View rowView= inflater.inflate(R.layout.listview_item_check, null, true);
        rowView.setMinimumHeight(60);

        TextView txtKet   = (TextView) rowView.findViewById(R.id.txtCheckListJenis);
        ImageView imgIcon = (ImageView) rowView.findViewById(R.id.imgCheckListIcon);

        txtKet.setText(ket[position]);
        switch (gbr[position]) {
            case 1:
                imgIcon.setImageResource(R.drawable.jenis_1);
                break;

            case 2:
                imgIcon.setImageResource(R.drawable.jenis_2);
                break;

            case 3:
                imgIcon.setImageResource(R.drawable.jenis_3);
                break;

            case 4:
                imgIcon.setImageResource(R.drawable.jenis_4);
                break;

            case 5:
                imgIcon.setImageResource(R.drawable.jenis_5);
                break;

            case 6:
                imgIcon.setImageResource(R.drawable.jenis_6);
                break;

            case 7:
                imgIcon.setImageResource(R.drawable.jenis_7);
                break;

        }

        return rowView;
    }

}
