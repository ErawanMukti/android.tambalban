package erris.tambalban;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChatPesan extends AppCompatActivity {

    SharedPreferences prefs;
    private String myJSON, auth_id;
    JSONArray data = null;
    String ip_address, id_pesan, id_penerima, id_pengirim, cur_date, member_pesan;
    ListView lv;
    EditText txtPesan;
    Button btnKirim;
    Bundle extras;
    ChatAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_pesan);

        prefs       = getSharedPreferences("app", Context.MODE_PRIVATE);
        extras      = getIntent().getExtras();
        ip_address  = getResources().getString(R.string.ip_address);
        id_pesan    = extras.getString("id_pesan");
        id_pengirim = extras.getString("pengirim");
        id_penerima = extras.getString("penerima");
        auth_id     = prefs.getString("auth_id", "");
        lv          = (ListView) findViewById(R.id.listCPesan);
        txtPesan    = (EditText) findViewById(R.id.txtCPesanText);
        btnKirim    = (Button) findViewById(R.id.btnCPesanKirim);
        Calendar cl = Calendar.getInstance();
        SimpleDateFormat track_time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cur_date    = track_time.format(cl.getTime());

        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!txtPesan.getText().toString().equals("")) {
                    member_pesan = txtPesan.getText().toString();
                    kirim_pesan(member_pesan, cur_date);
                }
            }
        });

        GetDataJSON task = new GetDataJSON();
        task.execute();

    }

    class GetDataJSON extends AsyncTask<String, Void, String> {
        //private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //loadingDialog = ProgressDialog.show(InfoLokasi.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("id_pesan", id_pesan));
            nameValuePairs.add(new BasicNameValuePair("id_pengirim", id_pengirim));
            nameValuePairs.add(new BasicNameValuePair("id_penerima", id_penerima));

            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "get_pesan_member.php");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            //loadingDialog.dismiss();

            if (result.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Data pesan tidak ditemukan", Toast.LENGTH_LONG).show();
            } else {
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("id");
                    JSONObject _id = data.getJSONObject(0);
                    id_pesan = _id.getString("id_pesan");

                    data = jsonObj.getJSONArray("hasil");
                    ArrayList<Integer> gambar = new ArrayList<Integer>();
                    ArrayList<String> pesan = new ArrayList<String>();
                    ArrayList<String> tanggal = new ArrayList<String>();

                    for(int i=0; i<data.length(); i++){
                        JSONObject c = data.getJSONObject(i);
                        pesan.add(c.getString("pesan"));
                        tanggal.add(c.getString("tanggal"));

                        if (c.getString("id_member").equals(auth_id)) {
                            gambar.add(R.drawable.in_message_bg);
                        } else {
                            gambar.add(R.drawable.out_message_bg);
                        }
                    }

                    adapter = new ChatAdapter(ChatPesan.this, tanggal, pesan, gambar);
                    lv.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void kirim_pesan(String pesan, String tanggal) {
        class ClsAsync extends AsyncTask<String, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Toast.makeText(ChatPesan.this, "Mengirim Pesan", Toast.LENGTH_SHORT).show();
            }

            @Override
            protected String doInBackground(String... params) {
                String pesan   = params[0];
                String tanggal = params[1];

                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id_pesan", id_pesan));
                nameValuePairs.add(new BasicNameValuePair("id_member", auth_id));
                nameValuePairs.add(new BasicNameValuePair("pesan", pesan));
                nameValuePairs.add(new BasicNameValuePair("tanggal", tanggal));

                String result = null;
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(ip_address + "tambah_pesan_member.php");
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {

                if (result.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, silahkan ulangi", Toast.LENGTH_LONG).show();
                } else {
                    String s = result.trim();
                    if (s.equalsIgnoreCase("Berhasil")) {
                        Toast.makeText(ChatPesan.this, "Pesan Berhasil Dikirim", Toast.LENGTH_SHORT).show();

                        txtPesan.setText("");
                        adapter.add(cur_date, member_pesan, R.drawable.in_message_bg);
                        adapter.notifyDataSetChanged();
                    } else {
                        //Development
                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();

                        //Production
                        //Toast.makeText(getApplicationContext(), "Pendaftaran gagal. Harap periksa", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
        ClsAsync task = new ClsAsync();
        task.execute(pesan, tanggal);
    }
}
