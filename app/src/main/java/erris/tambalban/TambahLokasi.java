package erris.tambalban;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class TambahLokasi extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    SharedPreferences prefs;
    EditText txtNama, txtAlamat, txtTelp, txtLat, txtLng, txtJamB, txtJamT, txtTarif;
    Button btnSimpan;
    LinearLayout layoutJk, layoutTb;
    double lat, lng;
    String ip_address, auth_id, auth_akses;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Double curLat, curLng;
    private String myJSON;
    JSONArray data = null;
    ArrayList<String> selected_jk, selected_tb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_lokasi);

        prefs      = getSharedPreferences("app", Context.MODE_PRIVATE);
        ip_address = getResources().getString(R.string.ip_address);
        txtNama    = (EditText) findViewById(R.id.txtTLokasiNama);
        txtAlamat  = (EditText) findViewById(R.id.txtTLokasiAlamat);
        txtLat     = (EditText) findViewById(R.id.txtTLokasiLat);
        txtLng     = (EditText) findViewById(R.id.txtTLokasiLng);
        txtTelp    = (EditText) findViewById(R.id.txtTLokasiTelp);
        txtJamB    = (EditText) findViewById(R.id.txtTLokasiJamB);
        txtJamT    = (EditText) findViewById(R.id.txtTLokasiJamT);
        txtTarif   = (EditText) findViewById(R.id.txtTLokasiTarif);
        btnSimpan  = (Button) findViewById(R.id.btnTLokasiSave);
        layoutJk   = (LinearLayout) findViewById(R.id.layoutTLokasiJk);
        layoutTb   = (LinearLayout) findViewById(R.id.layoutTLokasiTb);
        auth_id    = prefs.getString("auth_id", "");
        auth_akses = prefs.getString("auth_akses", "");
        selected_jk= new ArrayList<String>();
        selected_tb= new ArrayList<String>();

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(TambahLokasi.this)
                    .addConnectionCallbacks(TambahLokasi.this)
                    .addOnConnectionFailedListener(TambahLokasi.this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }

        GetDataJSON task = new GetDataJSON();
        task.execute();

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtAlamat.getText().equals("")) {
                    Toast.makeText(getBaseContext(), "Kolom alamat tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(txtLat.getText().equals("") || txtLng.getText().equals("")) {
                    Toast.makeText(getBaseContext(), "Kolom Lat/Lng tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                simpan_lokasi(txtNama.getText().toString(), txtAlamat.getText().toString(), txtLat.getText().toString(), txtLng.getText().toString(), txtTelp.getText().toString(), txtJamB.getText().toString(), txtJamT.getText().toString(), txtTarif.getText().toString(), selected_jk, selected_tb);
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            curLat = mLastLocation.getLatitude();
            curLng = mLastLocation.getLongitude();
        } else {
            curLat = 0.00;
            curLng = 0.00;
        }
        txtLat.setText(curLat.toString());
        txtLng.setText(curLng.toString());
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    void getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(this);
        List<Address> address;

        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address.size() > 0) {
                Address location=address.get(0);
                lat = location.getLatitude();
                lng = location.getLongitude();
            } else {
                lat = 0; lng = 0;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void simpan_lokasi(String nama, String alamat, String lat, String lng, String telp, String jamb, String jamt, String tarif, ArrayList<String> jk, ArrayList<String> tb) {
        class ClsAsync extends AsyncTask<String, Void, String> {
            private Dialog loadingDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadingDialog = ProgressDialog.show(TambahLokasi.this, "Harap Tunggu", "Loading...");
            }

            @Override
            protected String doInBackground(String... params) {
                String nama   = params[0];
                String alamat = params[1];
                String lat    = params[2];
                String lng    = params[3];
                String telp   = params[4];
                String jamb   = params[5];
                String jamt   = params[6];
                String tarif  = params[7];
                String jk     = params[8];
                String tb     = params[9];

                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("nama", nama));
                nameValuePairs.add(new BasicNameValuePair("alamat", alamat));
                nameValuePairs.add(new BasicNameValuePair("lat", lat));
                nameValuePairs.add(new BasicNameValuePair("lng", lng));
                nameValuePairs.add(new BasicNameValuePair("telp", telp));
                nameValuePairs.add(new BasicNameValuePair("jamb", jamb));
                nameValuePairs.add(new BasicNameValuePair("jamt", jamt));
                nameValuePairs.add(new BasicNameValuePair("tarif", tarif));
                nameValuePairs.add(new BasicNameValuePair("jk", jk));
                nameValuePairs.add(new BasicNameValuePair("tb", tb));
                nameValuePairs.add(new BasicNameValuePair("id", auth_id));
                nameValuePairs.add(new BasicNameValuePair("akses", auth_akses));

                String result = null;
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(ip_address + "tambah_lokasi.php");
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                loadingDialog.dismiss();

                if (result.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, silahkan ulangi", Toast.LENGTH_LONG).show();
                } else {
                    String s = result.trim();
                    if (s.equalsIgnoreCase("Berhasil")) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(TambahLokasi.this);
                        dialog.setCancelable(false);
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                finish();
                                return;
                            }

                        });

                        dialog.setMessage("Selamat. Penambahan lokasi berhasil, kami akan segera melakukan proses validasi");
                        AlertDialog alert = dialog.create();
                        alert.show();
                    } else {
                        //Development
                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();

                        //Production
                        //Toast.makeText(getApplicationContext(), "Pendaftaran gagal. Harap periksa", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }

        String str_jk = "", str_tb = "";

        for(int i=0; i<jk.size(); i++) {
            str_jk += jk.get(i) + ",";
        }
        for(int i=0; i<tb.size(); i++) {
            str_tb += tb.get(i) + ",";
        }

        ClsAsync task = new ClsAsync();
        task.execute(nama, alamat, lat, lng, telp, jamb, jamt, tarif, str_jk, str_tb);
    }

    class GetDataJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(TambahLokasi.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "get_all_jenis.php");
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data jenis tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("arr_jk");

                    final String[] id_jk   = new String[data.length()];
                    final String[] nama_jk = new String[data.length()];

                    for(int i=0; i<data.length(); i++){
                        JSONObject c = data.getJSONObject(i);
                        id_jk[i]     = c.getString("id_jenis");
                        nama_jk[i]   = c.getString("nama_jenis");

                        CheckBox chk_jk = new CheckBox(TambahLokasi.this);
                        chk_jk.setText(nama_jk[i]);
                        layoutJk.addView(chk_jk);

                        chk_jk.setOnCheckedChangeListener(handleJkCheck(id_jk[i]));
                    }

                    data = jsonObj.getJSONArray("arr_tb");

                    final String[] id_tb   = new String[data.length()];
                    final String[] nama_tb = new String[data.length()];

                    for(int i=0; i<data.length(); i++){
                        JSONObject c = data.getJSONObject(i);
                        id_tb[i]     = c.getString("id_jenis");
                        nama_tb[i]   = c.getString("nama_jenis");

                        CheckBox chk_tb = new CheckBox(TambahLokasi.this);
                        chk_tb.setText(nama_tb[i]);
                        layoutTb.addView(chk_tb);

                        chk_tb.setOnCheckedChangeListener(handleTbCheck(id_tb[i]));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected CompoundButton.OnCheckedChangeListener handleJkCheck(final String idx) {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_jk.add(idx);
                } else {
                    selected_jk.remove(idx);
                }
            }
        };
    }

    protected CompoundButton.OnCheckedChangeListener handleTbCheck(final String idx) {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selected_tb.add(idx);
                } else {
                    selected_tb.remove(idx);
                }
            }
        };
    }
}
