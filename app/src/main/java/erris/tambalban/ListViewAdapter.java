package erris.tambalban;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListViewAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] alamat, nama, status;

    public ListViewAdapter(Activity context, String[] alamat, String[] nama, String[] status) {
        super(context, R.layout.listview_item_list, alamat);
        this.context = context;
        this.alamat  = alamat;
        this.nama    = nama;
        this.status  = status;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return getView(position, convertView, parent);
    }
    
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View rowView= inflater.inflate(R.layout.listview_item_list, null, true);
        rowView.setMinimumHeight(60);

        TextView txtAlamat = (TextView) rowView.findViewById(R.id.txtListItemAlamat);
        TextView txtNama   = (TextView) rowView.findViewById(R.id.txtListItemNama);
        TextView txtStatus = (TextView) rowView.findViewById(R.id.txtListItemStatus);

        txtAlamat.setText(alamat[position]);
        txtNama.setText(nama[position]);
        txtStatus.setText(status[position]);

        return rowView;
    }

}
