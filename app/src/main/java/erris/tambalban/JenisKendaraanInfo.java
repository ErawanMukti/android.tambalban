package erris.tambalban;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class JenisKendaraanInfo extends AppCompatActivity {

    EditText txtNama, txtDeskripsi;
    Button btnSave;
    Spinner spnIcon;
    ImageView imgIcon;
    String ip_address, id_jk;
    Bundle extras;
    private String myJSON;
    JSONArray data = null;
    String[] arr_icon;
    int idx_jenis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jenis_kendaraan_info);

        ip_address   = getResources().getString(R.string.ip_address);
        extras       = getIntent().getExtras();
        id_jk        = extras.getString("id_jenis");
        txtNama      = (EditText) findViewById(R.id.txtJkNama);
        txtDeskripsi = (EditText) findViewById(R.id.txtJkDeskripsi);
        spnIcon      = (Spinner) findViewById(R.id.spnJkIcon);
        imgIcon      = (ImageView) findViewById(R.id.imgJkIcon);
        btnSave      = (Button) findViewById(R.id.btnJKSave);
        arr_icon     = getResources().getStringArray(R.array.arr_icon);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, arr_icon);
        spnIcon.setAdapter(adapter);

        spnIcon.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                idx_jenis = i + 1;
                switch (i){
                    case 0:
                        imgIcon.setImageResource(R.drawable.jenis_1);
                        break;

                    case 1:
                        imgIcon.setImageResource(R.drawable.jenis_2);
                        break;

                    case 2:
                        imgIcon.setImageResource(R.drawable.jenis_3);
                        break;

                    case 3:
                        imgIcon.setImageResource(R.drawable.jenis_4);
                        break;

                    case 4:
                        imgIcon.setImageResource(R.drawable.jenis_5);
                        break;

                    case 5:
                        imgIcon.setImageResource(R.drawable.jenis_6);
                        break;

                    case 6:
                        imgIcon.setImageResource(R.drawable.jenis_7);
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (id_jk.equals("")) {
            btnSave.setText("Tambah Jenis Kendaraan");
        } else {
            btnSave.setText("Ubah Jenis Kendaraan");

            GetDataJSON task = new GetDataJSON();
            task.execute();
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtNama.getText().toString().equals("")) {
                    Toast.makeText(getBaseContext(), "Kolom jenis kendaraan tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                simpan_jk(id_jk, txtNama.getText().toString(), txtDeskripsi.getText().toString(), String.valueOf(idx_jenis));
            }
        });
    }

    private void simpan_jk(String id, String nama, String deskripsi, String icon) {
        class ClsAsync extends AsyncTask<String, Void, String> {
            private Dialog loadingDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loadingDialog = ProgressDialog.show(JenisKendaraanInfo.this, "Harap Tunggu", "Loading...");
            }

            @Override
            protected String doInBackground(String... params) {
                String id        = params[0];
                String nama      = params[1];
                String deskripsi = params[2];
                String icon      = params[3];

                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", id));
                nameValuePairs.add(new BasicNameValuePair("nama", nama));
                nameValuePairs.add(new BasicNameValuePair("deskripsi", deskripsi));
                nameValuePairs.add(new BasicNameValuePair("icon", icon));

                String result = null;
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(ip_address + "tambah_jenis_kendaraan.php");
                    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    is = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    result = sb.toString();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            protected void onPostExecute(String result) {
                loadingDialog.dismiss();

                if (result.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Terjadi kesalahan, silahkan ulangi", Toast.LENGTH_LONG).show();
                } else {
                    String s = result.trim();
                    if (s.equalsIgnoreCase("Berhasil")) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(JenisKendaraanInfo.this);
                        dialog.setCancelable(false);
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // TODO Auto-generated method stub
                                finish();
                                return;
                            }

                        });

                        dialog.setMessage("Selamat. Penambahan jenis kendaraan berhasil");
                        AlertDialog alert = dialog.create();
                        alert.show();
                    } else {
                        //Development
                        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();

                        //Production
                        //Toast.makeText(getApplicationContext(), "Pendaftaran gagal. Harap periksa", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
        ClsAsync task = new ClsAsync();
        task.execute(id, nama, deskripsi, icon);
    }

    class GetDataJSON extends AsyncTask<String, Void, String> {
        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingDialog = ProgressDialog.show(JenisKendaraanInfo.this, "Harap Tunggu", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            String result = null;

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("id", id_jk));

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(ip_address + "get_jenis_kendaraan.php");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result){
            loadingDialog.dismiss();

            if(result.isEmpty()){
                Toast.makeText(getApplicationContext(), "Data jenis kendaraan tidak ditemukan", Toast.LENGTH_LONG).show();
            }else{
                String s = result.trim();
                myJSON = s;

                try {
                    JSONObject jsonObj = new JSONObject(myJSON);
                    data = jsonObj.getJSONArray("hasil");
                    JSONObject c = data.getJSONObject(0);

                    txtNama.setText(c.getString("nama_jenis"));
                    txtDeskripsi.setText(c.getString("deskripsi"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
